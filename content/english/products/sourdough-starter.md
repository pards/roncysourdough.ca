---
title: "Sourdough Starter"
date: 2024-06-09T11:22:16+06:00
images: 
  - "images/showcase/sourdough-starter.jpg"
  - "images/showcase/boule.jpg"
  - "images/showcase/slices.jpg"

# meta description
description : "Super active sourdough starter that makes great bread. Descended from Carl's 1847 Oregon Trail starter, this variant has been fed daily (not refrigerated) and is well-adapted to the Canadian climate."

# product Price
price: "20.00"
discount_price: "15.00"

# product variation
#colors : ["black","white","gray"]
#sizes : ["small","medium","large"]

# button link if you don't want to use snipcart. empty link will not show button
button_link: ""

draft: false
---

Super active sourdough starter that makes great bread. Descended from Carl's 1847 Oregon Trail starter, this variant has been fed daily (not refrigerated) and is well-adapted to the Canadian climate.

[Order now](https://roncysourdough.etsy.com) direct from our Etsy store.



<video width="320" height="240" controls>
    <source src="/videos/sourdough-starter-growth.mp4" type="video/mp4" />
    Your browser does not support the video tag.
</video>

Time-lapse video showing growth rate of starter over 6 hours