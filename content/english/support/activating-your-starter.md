---
title: "Activating Your Starter"
---

## Read First

You'll need the following items to revive your starter:

* The dehydrated starter you received.
* 1 small jar, about 4oz/120ml in size.
* 1 large jar, about 12oz/350ml in size.
* At least 2 cups of all-purpose flour.
* Room-temperature water.

## Step 1

In a small container (about 4oz/120ml in size), mix one tablespoon of lukewarm water and half a teaspoon of your starter. Let stand for a few minutes to soften the starter granules. Then mix in one tablespoon of flour. 

Depending on the flour, you may need to add an additional teaspoon or two of water. You want the mixture to have the consistency pancake batter.  

Place in a warm place 70°F - 85°F or 21°C - 29°C for about 4-12 hours or until the mixture gets bubbly (whichever happens first).

## Step 2

Move your starter to a larger container (about 12oz/350ml in size) and stir in 1/4 cup of water and 1/4 cup of flour.  

Place in a warm place 70°F - 85°F or 21°C - 29°C for about 4-12 hours or until the mixture gets bubbly (whichever happens first).

## Step 3

Add 1/2 cup of water and 1/2 cup of flour. 

Place in a warm place 70°F - 85°F or 21°C - 29°C for about 4-12 hours or until the mixture gets bubbly (whichever happens first).

### Ready to bake

When this bubbles up, you will have about one cup of very active starter that is ready for use, or for storage in your refrigerator. 

The time between refreshments will depend mainly on temperature. You can expect the first sign of starter activity to take from four to 12 hours.

### Tips:

- The water should feel neither warm nor cold - about body temperature is perfect.

- A baby food jar and a 12-ounce peanut butter jar work well for the small and large containers.

- Established starter will do fine in any room temperature that is comfortable for humans.  Warmer room temperature is helpful when reviving start, but do not go over 85°F or 29°C if at all possible.  Cooler temperatures just extend the time required.  If room temperature is under 68F, I find a warmer spot such as the top of my refrigerator or a cold oven with the light on.
- If your starter doesn’t bubble within 12 hours, just go to the second step and add more flour and water. 

- Some tap water has too much chlorine so filtered water can be helpful. Keep adding flour and water every day until it bubbles. Old starters can take a week of this to revive so don’t give up until then.

- The starter grows best between 70°F - 85°F or 21°C - 29°C. The starter is sensitive to high temperatures and will die.